import { expect, tap } from '@push.rocks/tapbundle';
import * as deesElement from '../ts/index.js';

tap.test('should create a static element', async () => {
  @deesElement.customElement('my-button')
  class MyButton extends deesElement.DeesElement {
    // STATIC
    public static styles = [
      deesElement.cssManager.defaultStyles,
      deesElement.css`
      .buttonClass {
        background: ${deesElement.cssManager.bdTheme('blue', 'black')};
      }
    `,
    ];

    // INSTANCE
    render() {
      return deesElement.html`<div class="buttonClass">My Button</div>`;
    }
  }
});

tap.start();
