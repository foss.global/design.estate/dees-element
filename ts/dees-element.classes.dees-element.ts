import * as plugins from './dees-element.plugins.js';

export class DeesElement extends plugins.lit.LitElement {
  // INSTANCE
  @plugins.lit.property({ type: Boolean })
  public goBright: boolean = false;

  // domtools
  public domtoolsPromise: Promise<plugins.domtools.DomTools>;

  @plugins.lit.property()
  domtools?: plugins.domtools.DomTools;

  public rxSubscriptions: plugins.smartrx.rxjs.Subscription[] = [];
  private themeSubscription: plugins.smartrx.rxjs.Subscription;

  private elementDomReadyDeferred = plugins.domtools.plugins.smartpromise.defer();
  public elementDomReady = this.elementDomReadyDeferred.promise;

  constructor(optionsArg: plugins.domtools.IDomToolsContructorOptions = {}) {
    super();
    this.domtoolsPromise = plugins.domtools.elementBasic.setup(this, optionsArg);
    this.domtoolsPromise.then((domtoolsArg) => {
      this.domtools = domtoolsArg;
    });
  }

  public async connectedCallback() {
    super.connectedCallback();
    const domtools = await this.domtoolsPromise;
    this.themeSubscription = domtools.themeManager.themeObservable.subscribe((goBrightArg) => {
      this.goBright = goBrightArg;
    });
    this.rxSubscriptions.push(this.themeSubscription);
    this.dispatchEvent(new CustomEvent('deesElementConnected'));
  }

  public firstUpdated(_changedProperties: Map<string | number | symbol, unknown>): void {
    super.firstUpdated(_changedProperties);
    this.elementDomReadyDeferred.resolve();
  }

  public async disconnectedCallback() {
    await this.domtoolsPromise;
    super.disconnectedCallback();
    for (const subscription of this.rxSubscriptions) {
      subscription.unsubscribe();
    }
    this.dispatchEvent(new CustomEvent('deesElementDisconnected'));
  }
}
