import { CssManager } from './dees-element.classes.cssmanager.js';

// lit exports
export { html, type TemplateResult, css, unsafeCSS, render, type CSSResult } from 'lit';
export { unsafeHTML } from 'lit/directives/unsafe-html.js';

export { customElement } from 'lit/decorators/custom-element.js';

export { property, state, query, queryAll, queryAsync } from 'lit/decorators.js';

export { until } from 'lit/directives/until.js';
export { asyncAppend } from 'lit/directives/async-append.js';

// domtools exports
import * as domtools from '@design.estate/dees-domtools';
export { domtools };

// DeesElements exports
export * from './dees-element.classes.dees-element.js';
export * from './dees-element.classes.subscribedirective.js';
export * from './dees-element.classes.resolvedirective.js';

/**
 * a singleton instance of CssManager
 */
export const cssManager = new CssManager();
