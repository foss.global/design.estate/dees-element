/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@design.estate/dees-element',
  version: '2.0.25',
  description: 'a custom element class extending lit element class'
}
