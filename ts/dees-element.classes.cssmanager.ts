import { CSSResult, unsafeCSS } from 'lit';
import * as plugins from './dees-element.plugins.js';
import * as domtools from '@design.estate/dees-domtools';

export interface IBdVarTriplet {
  cssVarName: string;
  darkValue: string;
  brightValue: string;
}

export class CssManager {
  public domtoolsPromise = domtools.DomTools.setupDomTools();
  public goBright: boolean = false;
  public bdVarTripletStore: IBdVarTriplet[] = [];

  constructor() {
    this.domtoolsPromise.then(async (domtoolsArg) => {
      domtoolsArg.themeManager.themeObservable.subscribe(async (goBrightArg) => {
        this.goBright = goBrightArg;
        await domtoolsArg.domReady.promise;
        for (const bdTripletArg of this.bdVarTripletStore) {
          document.body.style.setProperty(
            bdTripletArg.cssVarName,
            this.goBright ? bdTripletArg.brightValue : bdTripletArg.darkValue
          );
        }
      });
    });
  }

  public get defaultStyles() {
    return domtools.elementBasic.staticStyles;
  }

  public cssForDesktop(contentArg: CSSResult) {
    return unsafeCSS(domtools.breakpoints.cssForDesktop(contentArg));
  }

  public cssForNotebook(contentArg: CSSResult) {
    return unsafeCSS(domtools.breakpoints.cssForNotebook(contentArg));
  }

  public cssForTablet(contentArg: CSSResult) {
    return unsafeCSS(domtools.breakpoints.cssForTablet(contentArg));
  }

  public cssForPhablet(contentArg: CSSResult) {
    return unsafeCSS(domtools.breakpoints.cssForPhablet(contentArg));
  }

  public cssForPhone(contentArg: CSSResult) {
    return unsafeCSS(domtools.breakpoints.cssForPhone(contentArg));
  }

  public bdTheme(brightValueArg: string, darkValueArg: string): CSSResult {
    let returnCssVar: string;

    // lets determine the default value for quick page rendering.
    let defaultValue: string;
    if (domtools.DomTools.getGlobalDomToolsSync()) {
      defaultValue = domtools.DomTools.getGlobalDomToolsSync().themeManager.goBrightBoolean
        ? brightValueArg
        : darkValueArg;
    } else {
      defaultValue = darkValueArg;
    }

    const existingTriplet = this.bdVarTripletStore.find(
      (tripletArg) =>
        tripletArg.darkValue === darkValueArg && tripletArg.brightValue === brightValueArg
    );
    if (existingTriplet) {
      returnCssVar = existingTriplet.cssVarName;
    } else {
      const newTriplet: IBdVarTriplet = {
        cssVarName: `--${plugins.isounique.uni()}`,
        brightValue: brightValueArg,
        darkValue: darkValueArg,
      };
      this.bdVarTripletStore.push(newTriplet);

      this.domtoolsPromise.then(async (domtoolsArg) => {
        await domtoolsArg.domReady.promise;
        document.body.style.setProperty(newTriplet.cssVarName, defaultValue);
      });
      returnCssVar = newTriplet.cssVarName;
    }
    return plugins.lit.unsafeCSS(`var(${returnCssVar}, ${defaultValue})`);
  }

  public cssGridColumns = (amountOfColumnsArg: number, gapSizeArg: number): CSSResult => {
    let returnString = ``;
    for (let i = 0; i < amountOfColumnsArg; i++) {
      returnString += ` calc((100%/${amountOfColumnsArg}) - (${
        gapSizeArg * (amountOfColumnsArg - 1)
      }px/${amountOfColumnsArg}))`;
    }
    return plugins.lit.unsafeCSS(returnString);
  };
}
